;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (petri ui)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (ice-9 match)
  #:use-module (ice-9 threads)
  #:use-module (petri)
  #:use-module (petri config)
  #:use-module (petri graphviz)
  #:use-module (sdl2)
  #:use-module (sdl2 events)
  #:use-module (sdl2 image)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module (sdl2 ttf)
  #:use-module (sdl2 video)
  #:export (run-petri-with-ui))

(define queue '())
(define queue-mutex (make-mutex))
(define current-ui-thread #f)
(define current-petri-thread #f)

(define (alist-set lst key value)
  (let loop ((lst lst))
    (match lst
      (() (list (cons key value)))
      (((k . v) lst ...)
       (if (equal? k key)
           (cons (cons key value) lst)
           (cons (cons k v) (loop lst)))))))

(define (draw state g renderer font)
  (set-render-draw-color renderer 255 255 255 0)
  (clear-renderer renderer)
  (render-graph g state renderer font)
  (present-renderer renderer))

(define (update-ui-state state transitions msg)
  (define (find-transition-by-id id)
    (let loop ((trs transitions))
      (match trs
        (() #f)
        (((tr . i) trs ...)
         (if (= i id)
             tr
             (loop trs))))))

  (match msg
    (('transition . num)
     (let* ((tr (find-transition-by-id num))
            (input-places (transition-inputs tr)))
       (let loop ((state state) (inputs input-places))
         (match inputs
           (() state)
           ((in inputs ...)
            (loop (alist-set state in (- (or (assoc-ref state in) 0) 1))
                  inputs))))))
    (('transition-done . num)
     (let* ((tr (find-transition-by-id num))
            (output-places (transition-outputs tr)))
       (let loop ((state state) (outputs output-places))
         (match outputs
           (() state)
           ((out outputs ...)
            (loop (alist-set state out (+ (or (assoc-ref state out) 0) 1))
                  outputs))))))
    (_ state)))

(define (ui-loop transitions state)
  (define msg #f)
  (define g (create-graph (map car transitions)))
  (define font (load-font (string-append image-dir "/LiberationSans-Regular.ttf")
                          12))

  (lambda (renderer)
    (let loop ()
      (with-mutex queue-mutex
        (unless (null? queue)
          (set! msg (car queue))
          (set! state (update-ui-state state transitions msg))
          (set! queue (cdr queue))))
      (let ((event (poll-event)))
        (unless (or (equal? msg 'stop) (quit-event? event))
          (catch #t
            (lambda ()
              (draw state g renderer font))
            (lambda ( . e)
              (pk 'exception-in-loop e)))
          (usleep 20000)
          (loop))))))

(define (ui-thread transitions state)
  (set! current-ui-thread (current-thread))
  (sdl-init)
  (ttf-init)
  (call-with-window (make-window)
    (lambda (window)
      (call-with-renderer (make-renderer window)
        (ui-loop transitions state))))
  (ttf-quit)
  (sdl-quit)
  #t)

(define (run-petri-with-ui id-channel transitions inits end-place)
  "Run a petri net represented by its transitions, initial places and end place.
This is the same as run-petri, but this procedure creates a UI thread that keeps
track of the global state of the petri net."
  (define state '())
  (let* ((tr-map (map (lambda (tr id) (cons tr id))
                      transitions
                      (iota (length transitions) 1)))
         (transitions (map (lambda (tr)
                             (make-transition
                               (transition-inputs tr)
                               (transition-check tr)
                               (lambda* ( . inputs)
                                 (with-mutex queue-mutex
                                   (set! queue
                                     (append
                                       queue
                                       (list
                                         (cons 'transition
                                               (assoc-ref tr-map tr))))))
                                 (let ((res (apply (transition-func tr) inputs)))
                                   (with-mutex queue-mutex
                                     (set! queue
                                       (append
                                         queue
                                         (list
                                           (cons 'transition-done
                                                 (assoc-ref tr-map tr))))))
                                   (sleep 1)
                                   res))
                               (transition-outputs tr)))
                           transitions)))

    ;; Set initial state
    (let loop ((inits inits))
      (match inits
        (() #t)
        (((place . _) inits ...)
         (set! state (alist-set state place (+ (or (assoc-ref state place) 0) 1)))
         (loop inits))))

    (run-fibers
      (lambda ()
        (make-thread ui-thread tr-map state)

        (let ((end-channel (petri-fiber id-channel transitions inits end-place)))
          (let loop ((timeout-channel (make-channel)))
            (spawn-fiber
              (lambda ()
                (sleep 1)
                (put-message timeout-channel 'timeout)))
            (perform-operation
              (choice-operation
                (wrap-operation
                  (get-operation end-channel)
                  (lambda _
                    (with-mutex queue-mutex
                      (set! queue '(stop)))))
                (wrap-operation
                  (get-operation timeout-channel)
                  (lambda _
                    (unless (thread-exited? current-ui-thread)
                      (loop (make-channel)))))))))))

    (join-thread current-ui-thread)))
