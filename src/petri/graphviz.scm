;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (petri graphviz)
  #:use-module (ice-9 match)
  #:use-module (petri config)
  #:use-module (petri)
  #:use-module (sdl2)
  #:use-module (sdl2 render)
  #:use-module (sdl2 surface)
  #:use-module (sdl2 ttf)
  #:export (create-graph render-graph))

(load-extension (string-append graphviz-lib "/guile/2.0/extensions/libgv_guile.so")
                "SWIG_init")
(define id 0)
(define (get-id)
  (set! id (+ id 1))
  id)

(define (create-graph transitions)
  (define (get-edges-from-tr nodes tr outputs)
    (match outputs
      ('() '())
      ((out outs ...)
       (begin
         (cons (edge (assoc-ref nodes out) tr)
               (get-edges-from-tr nodes tr outs))))))

  (define (get-edges-to-tr nodes inputs tr)
    (match inputs
      ('() '())
      ((in ins ...)
       (begin
         (cons (edge tr (assoc-ref nodes in))
               (get-edges-to-tr nodes ins tr))))))

  (define (get-edges nodes transitions)
    (match transitions
      ('() '())
      (((tr . node) trs ...)
       (append (get-edges-from-tr nodes node (transition-outputs tr))
               (get-edges-to-tr nodes (transition-inputs tr) node)
               (get-edges nodes trs)))))

  (let* ((g (digraph "g"))
         (places (map (lambda (place)
                        (let ((n (node g (symbol->string place))))
                          (setv n "type" "place")
                          `(,place . ,n)))
                      (apply append
                             (append
                               (map transition-inputs transitions)
                               (map transition-outputs transitions)))))
         (transitions (map (lambda (tr id)
                             (let ((n (node g (string-append "tr_" (number->string id)))))
                               (setv n "type" "transition")
                               `(,tr . ,n)))
                           transitions (iota (length transitions) 1)))
         (edges (get-edges (append places transitions) transitions)))
    (layout g "fdp")
    (render g)
    g))

(define (render-transition ren x y)
  (render-draw-line ren (- x 16) (- y 16) (- x 16) (+ y 16))
  (render-draw-line ren (- x 16) (+ y 16) (+ x 16) (+ y 16))
  (render-draw-line ren (+ x 16) (+ y 16) (+ x 16) (- y 16))
  (render-draw-line ren (+ x 16) (- y 16) (- x 16) (- y 16)))

(define (render-place ren x y)
  (render-draw-line ren (+ x 16) (+ y 00) (+ x 14) (+ y 08))
  (render-draw-line ren (+ x 14) (+ y 08) (+ x 08) (+ y 14))
  (render-draw-line ren (+ x 08) (+ y 14) (+ x 00) (+ y 16))
  (render-draw-line ren (+ x 00) (+ y 16) (- x 08) (+ y 14))
  (render-draw-line ren (- x 08) (+ y 14) (- x 14) (+ y 08))
  (render-draw-line ren (- x 14) (+ y 08) (- x 16) (+ y 00))
  (render-draw-line ren (- x 16) (+ y 00) (- x 14) (- y 08))
  (render-draw-line ren (- x 14) (- y 08) (- x 08) (- y 14))
  (render-draw-line ren (- x 08) (- y 14) (+ x 00) (- y 16))
  (render-draw-line ren (+ x 00) (- y 16) (+ x 08) (- y 14))
  (render-draw-line ren (+ x 08) (- y 14) (+ x 14) (- y 08))
  (render-draw-line ren (+ x 14) (- y 08) (+ x 16) (+ y 00)))

(define (render-graph g tokens ren font)
  (set-render-draw-color ren 25 25 25 0)
  (let* ((edges (list (firstedge g)))
         (edges (let loop ((edges edges))
                  (match (nextedge g (car edges))
                    ('() edges)
                    (e (loop (cons e edges))))))
         (nodes (list (firstnode g)))
         (nodes (let loop ((nodes nodes))
                 (match (nextnode g (car nodes))
                   ('() nodes)
                   (n (loop (cons n nodes)))))))

    ;; Draw edges
    (let loop ((edges edges))
      (if (not (null? edges))
        (let* ((edge (car edges))
               (pos (getv edge "pos"))
               (poss (string-split pos #\ )))
          (let loop2 ((previous-x #f) (previous-y #f) (positions poss))
            (unless (null? positions)
              (let* ((pos (car positions))
                     (pos (let ((s (string-split pos #\,))) (if (equal? (car s) "e") (cdr s) s)))
                     (x (inexact->exact (round (string->number (car pos)))))
                     (y (inexact->exact (round (string->number (cadr pos))))))
                (when (and previous-x previous-y)
                  (render-draw-line ren previous-x previous-y x y))
                (loop2 x y (cdr positions)))))
          (loop (cdr edges)))))

    ;; Draw nodes (places and transitions)
    (let loop ((nodes nodes))
      (if (not (null? nodes))
        (let* ((node (car nodes))
               (pos (string-split (getv node "pos") #\,))
               (x (inexact->exact (round (string->number (car pos)))))
               (y (inexact->exact (round (string->number (cadr pos))))))
          (if (equal? (getv node "type") "transition")
              (render-transition ren x y)
              (render-place ren x y))
          (unless (equal? (getv node "type") "transition")
            (let* ((surface (render-font-solid font (nameof node) (make-color 0 0 0 0)))
                   (height (surface-height surface))
                   (width (surface-width surface)))
              (render-copy ren
                           (surface->texture ren surface)
                           #:dstrect `(,(inexact->exact (round (- x (/ width 2))))
                                       ,(+ y 20)
                                       ,width ,height)))
            (let* ((node-tokens (or (assoc-ref tokens (string->symbol (nameof node))) 0))
                   (surface (render-font-solid font (number->string node-tokens)
                                               (make-color 0 0 0 0)))
                   (height (surface-height surface))
                   (width (surface-width surface)))
              (render-copy ren
                           (surface->texture ren surface)
                           #:dstrect `(,(inexact->exact (round (- x (/ width 2))))
                                       ,(inexact->exact (round (- y (/ height 2))))
                                       ,width ,height))))
          (loop (cdr nodes)))))))
