;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(define-module (petri)
  #:use-module (fibers)
  #:use-module (fibers channels)
  #:use-module (fibers operations)
  #:use-module (srfi srfi-1)
  #:use-module (srfi srfi-9)
  #:use-module (ice-9 match)
  #:export (run-petri
            petri-fiber
            make-transition
            transition-inputs
            transition-check
            transition-func
            transition-outputs))

(define-record-type token
  (make-token id value)
  token?
  (id token-id)
  (value token-value))

(define-record-type transition
  (make-transition inputs check func outputs)
  transition?
  (inputs  transition-inputs)
  (check   transition-check)
  (func    transition-func)
  (outputs transition-outputs))

(define (id-getter-fiber id-channel)
  (lambda ()
    (let loop ((id 1))
      (put-message id-channel id)
      (loop (+ id 1)))))

(define (writer channel value)
  (lambda ()
    (put-message channel value)))

(define (combine a . b)
  "Return the cartesian product of a variable number of sets."
  (match b
    ('() (map list a))
    ((b restb ...)
     (match a
       ('() '())
       ((a resta ...)
        (append
          (map (lambda (el) (cons* a el)) (apply combine b restb))
          (apply combine resta (cons b restb))))))))

(define (filter-token-duplicate lsts)
  "From a list of lists of tokens, filter those lists that contain the same
token twice."
  (filter
    (lambda (lst)
      (define (has-duplicate l)
        (cond
          ((null? l) #f)
          ((member (car l) (cdr l)) #t)
          (else (has-duplicate (cdr l)))))
      (not (has-duplicate lst)))
    lsts))

(define (input-product a . b)
  "Build the set of possible inputs from a variable number of tokens waiting on
the corresponding input."
  (filter-token-duplicate (apply combine a b)))

(define (filter-out-null inputs)
  (filter (lambda (e)
            (not (eq? (token-id e) 0)))
          inputs))

(define (try-f f product)
  "Try to run f on the set of possible inputs and return the first input list
that is accepted"
  (fold
    (lambda (elem acc)
      (if acc
        acc
        (catch #t
          (lambda ()
            (let ((r (apply f (map token-value (filter-out-null elem)))))
              (if r elem #f)))
          (lambda (key . parameters)
            #f))))
    #f
    product))

(define (eat elems lsts)
  "Remove the elems from lsts."
  (match lsts
    ('() '())
    ((la lb ...)
     (match elems
       ((a b ...)
        (cons (filter (lambda (e) (not (eq? (token-id e) (token-id a)))) la) (eat b lb)))))))

(define (add elem num lsts)
  "Add an element to the numth list in lsts."
  (if (eq? num 0)
    (match lsts
      ((a b ...) (cons (cons elem a) b)))
    (match lsts
      ((a b ...) (cons a (add elem (- num 1) b))))))

(define (limit lsts inputs)
  "Token id 0 cannot happen “naturally”, so use it to filter out inputs that we
don't want to use. The result of this procedure is used by try-f that expect
this sort of filtering to actually give the right set of inputs to the function."
  (define (my-limit lsts num inputs)
    (match lsts
      ('() '())
      ((a b ...)
       (if (member num inputs)
         (cons a (my-limit b (+ num 1) inputs))
         (cons (map (const (make-token 0 #f)) a) (my-limit b (+ num 1) inputs))))))
  (my-limit lsts 0 inputs))

(define (dispatch-res output-channels results)
  (if (not (null? results))
    (begin
      (spawn-fiber (writer (car output-channels) (car results)))
      (dispatch-res (cdr output-channels) (cdr results)))))

(define (filter-out-by-input transitions input)
  (filter (lambda (tr)
            (not (member input (transition-inputs tr))))
          transitions))

(define (filter-by-input transitions input)
  (filter (lambda (tr)
            (member input (transition-inputs tr)))
          transitions))

(define (inputs-of trs)
  (uniq (fold append '() (map transition-inputs trs))))

(define (make-monitors transitions)
  (define (make-monitor-aux transitions monitor rest-inputs)
    (match rest-inputs
      ('() (list transitions monitor))
      ((input inputs ...)
       (let ((transitions (filter-out-by-input transitions input))
             (adding (filter-by-input transitions input)))
         (make-monitor-aux transitions (append adding monitor) (append (inputs-of adding) inputs))))))
  (let loop ((transitions transitions) (monitors '()))
    (match transitions
      ('() monitors)
      ((tr trs ...)
       (let ((res (make-monitor-aux trs (list tr) (inputs-of (list tr)))))
         (loop (car res) (cons (cadr res) monitors)))))))

(define (interval n1 n2)
  (if (> n1 n2)
    '()
    (cons n1 (interval (+ n1 1) n2))))

(define (num-of-trs inputs trs)
  (define (num-of-trs-aux inputs trs num)
    (match trs
      ('() '())
      ((tr trs ...)
       (match inputs
        (() '())
        ((in ins ...)
         (if (eq? in tr)
           (cons num (num-of-trs-aux ins trs (+ num 1)))
           (num-of-trs-aux ins (cons tr trs) (+ num 1))))))))
  (num-of-trs-aux inputs trs 0))

(define (compute-result-fiber res func outputs id-channel)
  (lambda ()
      (dispatch-res outputs
                    (map (lambda (res) (make-token (new-id id-channel) res))
                         (apply func (map token-value res))))))

(define (monitor-fiber monitor id-channel)
  (lambda ()
    (define (hard-retry-f lsts inputs f func outputs)
      (let* ((product (apply input-product (limit lsts inputs)))
             (res (try-f f product)))
        (if res
          (begin
            (spawn-fiber (compute-result-fiber res func outputs id-channel))
            (hard-retry-f (eat res lsts) inputs f func outputs))
          lsts)))
    (define (T-combine lsts)
      (let* ((inputs (inputs-of monitor))
             (msg (perform-operation
                    (apply
                      choice-operation
                      (map
                        (lambda (input num)
                          (wrap-operation
                            (get-operation input)
                            (lambda (res)
                              `(("from" . ,num) ("res" . ,res)))))
                        inputs (interval 0 (- (length inputs) 1))))))
             (from (assoc-ref msg "from"))
             (res (assoc-ref msg "res"))
             (lsts (add res from lsts)))
        (let loop ((lsts lsts) (monitor monitor))
          (match monitor
            ('() (T-combine lsts))
            ((tr trs ...)
             (loop (hard-retry-f
                     lsts
                     (num-of-trs inputs (transition-inputs tr))
                     (transition-check tr)
                     (transition-func tr)
                     (transition-outputs tr))
                   trs))))))
    (T-combine (map (const '()) (inputs-of monitor)))))

(define (new-id channel)
  (get-message channel))
(define (get-global-state channel)
  (get-message channel))

(define (uniq lst)
  (let loop ((lst lst) (aux '()))
    (if (null? lst)
      aux
      (loop (cdr lst) (if (member (car lst) aux) aux (cons (car lst) aux))))))

(define (run-petri id-channel transitions inits end-place)
  (run-fibers
    (lambda ()
      (let ((end-channel (petri-fiber id-channel transitions inits end-place)))
        (get-message end-channel)))))

(define (petri-fiber id-channel transitions inits end-place)
  (spawn-fiber (id-getter-fiber id-channel))
  (let* ((places (uniq (apply append (append (map transition-inputs transitions) (map transition-outputs transitions)))))
         (places (map (lambda (place) (cons place (make-channel))) places))
         (inits (map (lambda (init)
                       (cons (assoc-ref places (car init))
                             (cadr init)))
                     inits))
         (transitions (map (lambda (transition)
                             (make-transition
                               (map (lambda (input) (assoc-ref places input))
                                    (transition-inputs transition))
                               (transition-check transition)
                               (transition-func transition)
                               (map (lambda (input) (assoc-ref places input))
                                    (transition-outputs transition))))
                           transitions)))
    (for-each
      (lambda (monitor)
        (spawn-fiber (monitor-fiber monitor id-channel)))
      (make-monitors transitions))
    (for-each
      (lambda (init)
        (spawn-fiber (writer (car init) (make-token (get-message id-channel) (cdr init)))))
      inits)
    (assoc-ref places end-place)))
