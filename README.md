Guile Petri
===========

Guile Petri lets you define [petri nets](https://en.wikipedia.org/wiki/Petri_Net)
to represent a multithreading program.

Petri nets are very easy to understand and they are pretty usefull to define
network protocols, but also all sorts of other multithreading applications.
This library lets you define petri nets and run them on top of guile-fibers.

Features
--------

For the complete list of features and how to use them, please read the
[manual](https://tyreunom.frama.io/guile-petri/documentation).

With this library, you can define petri nets and run them.  Each token in the
net holds a value and each transition you define between places has a
function that evaluates either to false if the transition doesn't fire or
a list of value corresponding to the tokens that go through the transition.

Additionally, a global channel can be used to find out what's the global state
of the net, which is pretty usefull in some protocols, e.g. in peer-to-peer
protocols that require peer exchange.

Planned Features
----------------

The library will also comes with a small program that helps you visualize the
petri net in function, either real-time as it is being used by your application
or interactively, where you can fire transitions at will with a mouse click.

Finally, the library will allow you to check for possible dead locks in your
petri net.  If none can be found, you can be sure that there will never be a
dead lock in your program!

Limitations
-----------

Currently some limitations apply.  I'd appreciate help and ideas on how to
lift them.

Transition functions must not block, except in some subtle conditions.
Generally speaking, they should also be as fast as possible, because they
are run in sequence on every possible set of inputs.

I will probably implement transitions differently in the future to allow
for blocking operations.
