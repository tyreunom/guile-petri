;;;; Copyright (C) 2020 Julien Lepiller <julien@lepiller.eu>
;;;; 
;;;; This library is free software; you can redistribute it and/or
;;;; modify it under the terms of the GNU Lesser General Public
;;;; License as published by the Free Software Foundation; either
;;;; version 3 of the License, or (at your option) any later version.
;;;; 
;;;; This library is distributed in the hope that it will be useful,
;;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
;;;; Lesser General Public License for more details.
;;;; 
;;;; You should have received a copy of the GNU Lesser General Public
;;;; License along with this library; if not, write to the Free Software
;;;; Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA
;;;; 

(use-modules
  ((guix licenses) #:prefix license:)
  (guix build-system gnu)
  (guix download)
  (guix git-download)
  (guix packages)
  (guix utils)
  (gnu packages autotools)
  (gnu packages graphviz)
  (gnu packages guile)
  (gnu packages guile-xyz)
  (gnu packages pkg-config)
  (gnu packages sdl)
  (gnu packages texinfo)
  (gnu packages tls))

(package
  (name "guile-petri")
  (version "1.0")
  (source
    (origin
      (method git-fetch)
      (uri (git-reference
             (url "https://framagit.org/tyreunom/guile-petri")
             (commit version)))
      (file-name (git-file-name name version))
      (sha256
       (base32
        "00l03j8ajkd1a7sg1zycbpdaz71mscrncw7rwjzqk2ia6j04rwxm"))))
  (build-system gnu-build-system)
  (arguments
   `(#:tests? #f))
  (inputs
   `(("graphviz" ,graphviz)
     ("guile" ,guile-3.0)
     ("guile-fibers" ,guile-fibers)
     ("guile-sdl2" ,guile3.0-sdl2)))
  (native-inputs
   `(("automake" ,automake)
     ("autoconf" ,autoconf)
     ("pkg-config" ,pkg-config)
     ("texinfo" ,texinfo)))
  (home-page "https://framagit.org/tyreunom/guile-petri")
  (synopsis "")
  (description "")
  (license license:gpl3+))
