(use-modules (haunt asset)
             (haunt html)
             (haunt page)
             (haunt post)
             (haunt reader)
             (haunt site)
             (haunt builder assets)
             (haunt builder atom)
             (haunt builder blog)
             (haunt reader skribe))

(define petri-theme
  (theme #:name "Guile-Petri"
         #:layout
         (lambda (site title body)
           `((doctype "html")
             (head
               (meta (@ (charset "utf-8")))
               (title ,(string-append title " — " (site-title site)))
               (link (@ (rel "stylesheet") (href "/css/petri.css"))))
             (body (@ (lang "en"))
               (header
                 (p (@ (class "title")) (a (@ (href "/")) "Guile-Petri"))
                 (ul
                   (li (a (@ (href "/documentation/")) "Documentation"))
                   (li (a (@ (href "https://framagit.org/tyreunom/guile-petri"))
                          (img (@ (src "/guile-petri/images/gitlab.svg") (width "1em"))) "Source code"))))
               (div (@ (id "content-block")) (div (@ (id "content")) ,body))
               (footer
                 (p "© Copyright 2019 Julien Lepiller")))))
         #:post-template
         (lambda (post)
           `((article
             (h1 ,(post-ref post 'title))
             (p (@ (class "by")) "by " ,(post-ref post 'author)
                " — " ,(date->string* (post-date post)))
             (div ,(post-sxml post)))))
         #:collection-template
         (lambda (site title posts prefix)
           (define (post-uri post)
             (string-append "/" (or prefix "")
                            (site-post-slug site post) ".html"))
           `(div
             (div (@ (id "post-list"))
              ,@(map (lambda (post)
                       `(div (@ (class "post"))
                          (p (@ (class "title"))
                             (a (@ (href ,(post-uri post)))
                                ,(post-ref post 'title)))
                          (p ,(date->string* (post-date post)))
                          (p ,(post-ref post 'summary))))
                  posts))))))

(define (index site posts)
  (define body
    `())
  (make-page "index.html"
             (with-layout petri-theme site "Guile-petri" body)
             sxml->html))

(site #:title "Guile-petri"
      #:domain "tyreunom.frama.io/guile-petri"
      #:default-metadata
      '((author . "Julien Lepiller")
        (email  . "julien@lepiller.eu"))
      #:readers (list sxml-reader skribe-reader)
      #:builders (list index
                       (atom-feed)
                       (atom-feeds-by-tag)
                       (static-directory "css")
                       (static-directory "images")))
